package designPattern;

class WinFactory extends GUIFactory
{
    public Button createButton()
    {
        return(new WinButton());
    }
}

class OSXFactory extends GUIFactory
{
    public Button createButton()
    {
        return(new OSXButton());
    }
}


 
class WinButton extends Button
{
    public void paint()
    {
        System.out.println("I'm a WinButton: "+ getCaption());
    }
}

class OSXButton extends Button
{
    public void paint()
    {
        System.out.println("I'm a OSXButton: "+ getCaption());
    }
}

