package designPattern;
import java.util.Random;

public abstract class GUIFactory
{
    public static GUIFactory getFactory()
    {
    	Random rand = new Random();
        int sys= rand.nextInt(5);
        
        if (sys == 0)
            return(new WinFactory());
        else
            return(new OSXFactory());
    }
    public abstract Button createButton();
}
