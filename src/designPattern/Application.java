package designPattern;

public class Application
{
    public static void main(String[] args)
    {
    	GUIFactory aFactory = GUIFactory.getFactory();
        Button aButton = aFactory.createButton();
        aButton.setCaption("Play");
        aButton.paint();
    }
    // affiche :
    //   I'm a WinButton: Play
    // ou :
    //   I'm a OSXButton: Play
}