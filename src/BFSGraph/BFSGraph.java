package BFSGraph;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import BFSGraph.AdjList;
import BFSGraph.Node;

	public class BFSGraph {
	int size;
	AdjList[] array;
	public BFSGraph(int size) {
		this.size=size;
		array = new AdjList[this.size];
		for(int i=0; i<size; i++) {
			array[i] = new AdjList();
			array[i].head=null;
		}
	}
	public void AddNode(int src, int dest) {
		Node n = new Node(dest,null);
		n.next = array[src].head;
		array[src].head=n;
	}
		public void BFSSearch(int startVertex, int search) {
			Boolean[] visited = new Boolean[size]; 
			Boolean isFound = false;
			for (int i = 0; i < size; i++) {
				visited[i]=false;
			}
			Queue<Integer> s = new LinkedList<Integer>();
			s.add(startVertex);
			while (! s.isEmpty()) {
				int n = s.poll();
				System.out.println("Visit : "+ n);
				visited[n] = true;
				Node head = array[n].head;
				Boolean isDone = true;
				while (head != null) {
					if (search == head.dest) {
						System.out.println("node is found ");
						isFound = true;
						break;
					}
					if (visited[head.dest] == false) {
						s.add(head.dest);
						visited[head.dest] = true;
					} else {
						head = head.next;
					}
					if (isFound == true) {
						break;
					}
				}
				}
			if (isFound == false) {
				System.out.println("node cannot found ");
		}
	}
		public void DFSExpore(int startVertex) {
		Boolean[] visited = new Boolean[size]; 
		for (int i = 0; i < size; i++) {
			visited[i]=false;
		}
		Queue<Integer> s = new LinkedList<Integer>();
		s.add(startVertex);
		while (! s.isEmpty()) {
			int n = s.poll();
			System.out.println("Visit : "+ n);
			visited[n] = true;
			Node head = array[n].head;
			Boolean isDone = true;
			while (head != null) {
				if (visited[head.dest] == false) {
					s.add(head.dest);
					visited[head.dest] = true;
				} else {
					head = head.next;
				}
			}
		
		}
	}
}
