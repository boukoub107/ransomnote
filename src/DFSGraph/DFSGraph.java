package DFSGraph;

import java.util.Stack;

public class DFSGraph {
int size;
AdjList[] array;
public DFSGraph(int size) {
	this.size=size;
	array = new AdjList[this.size];
	for(int i=0; i<size; i++) {
		array[i] = new AdjList();
		array[i].setHead(null);
	}
}
public void AddNode(int src, int dest) {
	Node n = new Node(dest,null);
	n.next = array[src].getHead();
	array[src].setHead(n);
}
	public void DFSSearch(int startVertex, int search) {
		Boolean[] visited = new Boolean[size]; 
		Boolean isFound = false;
		for (int i = 0; i < size; i++) {
			visited[i]=false;
		}
		Stack<Integer> s = new Stack<Integer>();
		s.push(startVertex);
		while (! s.isEmpty()) {
			int n = s.pop();
			s.push(n);
			visited[n] = true;
			Node head = array[n].getHead();
			Boolean isDone = true;
			while (head != null) {
				if (search == head.dest) {
					System.out.println("node is found ");
					isFound = true;
					break;
				}
				if (visited[head.dest] == false) {
					s.push(head.dest);
					visited[head.dest] = true;
					isDone = false;
					break;
				} else {
					head = head.next;
				}
				if (isFound == true) {
					break;
				}
			}
			if (isDone == true){
				int outN = s.pop();
				System.out.println("visit node : "+ outN);
			}
			}
		if (isFound == false) {
			System.out.println("node cannot found ");
	}
}
	public void DFSExpore(int startVertex) {
	Boolean[] visited = new Boolean[size]; 
	for (int i = 0; i < size; i++) {
		visited[i]=false;
	}
	Stack<Integer> s = new Stack<Integer>();
	s.push(startVertex);
	while (! s.isEmpty()) {
		int n = s.pop();
		s.push(n);
		visited[n] = true;
		Node head = array[n].getHead();
		Boolean isDone = true;
		while (head != null) {
			if (visited[head.dest] == false) {
				s.push(head.dest);
				visited[head.dest] = true;
				isDone = false;
				break;
			} else {
				head = head.next;
			}
		}
		if (isDone == true){
			int outN = s.pop();
			System.out.println("visit node : "+ outN);
		}
	}
}
}
