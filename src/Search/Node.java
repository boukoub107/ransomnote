package Search;
class Node 
{ 
    int key; 
    Node left, right; 
  
    public Node(int item) 
    { 
        key = item; 
        left = right = null; 
    }

	@Override
	public String toString() {
		return "Node=> key: " + key + ", left: " + left + ", right: " + right + ".";
	}

	public Node() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	} 
    
} 
  
