package Graph;

public class BinaryTree {
NodeTree root;

public BinaryTree(NodeTree root) {
	super();
	this.root = root;
}
public void AddNode(NodeTree newNode, NodeTree rootExplore) {
	if(rootExplore == null) 
		return;
	if(newNode.value > rootExplore.value) {
		if(rootExplore.right == null) {
			rootExplore.right=newNode;
		}
		else {
			AddNode(newNode, rootExplore.right);
		}
	}if(newNode.value < rootExplore.value) {
		if(rootExplore.left == null) {
			rootExplore.left=newNode;
		}
		else {
			AddNode(newNode, rootExplore.left);
		}
	}
}
public void SearchNode(int value, NodeTree rootExplore) {
	if(rootExplore == null) {
		System.out.println("Value is cannot founded!!");
		return;
	}
	if(value == rootExplore.value) {
		System.out.println("Value is founded!!");
		System.out.println("valur\t:"+rootExplore.value);
		System.out.println("left\t:"+rootExplore.left);
		System.out.println("right\t:"+rootExplore.right);
	}
	if(value > rootExplore.value) {
			SearchNode(value, rootExplore.right);
	}
	if(value < rootExplore.value) {
			SearchNode(value, rootExplore.left);
		
	}
}
}
